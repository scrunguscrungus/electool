// dllmain.cpp : Defines the entry point for the DLL application.
#include "pch.h"
#include <string>
#include "Hooking.Patterns.h"
#include <stdio.h>

static void SetupConsole()
{
    AllocConsole();
    FILE* pDummy;
    freopen_s(&pDummy, "CONIN$", "r", stdin);
    freopen_s(&pDummy, "CONOUT$", "w", stdout);
    freopen_s(&pDummy, "CONOUT$", "w", stderr);

    SetWindowText(GetConsoleWindow(), L"Electool Output");
}

template<class T>
void WriteToMemory(LPVOID addr, T buff, size_t size)
{
    DWORD old;
    VirtualProtect(addr, size, PAGE_EXECUTE_READWRITE, &old);
    memcpy(addr, (char*)&buff, size);
    VirtualProtect(addr, size, old, &old);
}

static void InstallHooks()
{
    printf("Electool: Retargeting the jump...\n");
    hook::pattern pattern;

    pattern = hook::pattern("72 ? 48 FF C2 48 8B 4D ? 48 8B C1 48 81 FA 00 10 00 00 72 ? 48 83 C2 27 48 8B 49 ? 48 2B C1 48 83 C0 F8 48 83 F8 1F 76 ? FF 15 ? ? ? ? CC E8 ? ? ? ? 48 C7 45 ? 0F 00 00 00 48 C7 45 ? 0A 00 00 00 F2 0F 10 05 ? ? ? ? F2 0F 11 45 ? 0F B7 05 ? ? ? ? 66 89 45 ? C6 45 ? 00 41 B8 64 00 00 00");
    
    if (pattern.count_hint(1).empty())
    {
        SetupConsole();
        printf("Couldn't find the pattern to hook :(\n");
        printf("The Skalters have won.\n");
        std::system("pause");
        return;
    }
    else
    {
        void* pLoc = pattern.get_first(1);
        printf("Pattern found at %p!\n", pLoc);
        WriteToMemory(pLoc, 0x73, 1);
        printf("%08X <-- This should be 00007372\n", *(short*)pattern.get_first(0));
        printf("Electool: Skalters vanquished!\n");
    }
}

BOOL APIENTRY DllMain( HMODULE hModule,
                       DWORD  ul_reason_for_call,
                       LPVOID lpReserved
                     )
{
    switch (ul_reason_for_call)
    {
    case DLL_PROCESS_ATTACH:
        //SetupConsole();
        InstallHooks();
    case DLL_THREAD_ATTACH:
    case DLL_THREAD_DETACH:
    case DLL_PROCESS_DETACH:
        break;
    }
    return TRUE;
}

