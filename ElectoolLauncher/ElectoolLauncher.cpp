#ifdef _WIN32
#include <Windows.h>
#endif

#include <iostream>
#include <filesystem>

constexpr const char* pszDLL = "Electool.dll";
void Pause()
{
	//TODO: Crossplat
#ifdef _WIN32
	std::system("pause");
#endif
}

#ifdef _WIN32
std::string GetErrorString(DWORD err)
{
	if (err == 0)
		return std::string("No error");

	LPSTR msgBuf = nullptr;
	size_t s = FormatMessageA(
		FORMAT_MESSAGE_ALLOCATE_BUFFER | FORMAT_MESSAGE_FROM_SYSTEM | FORMAT_MESSAGE_IGNORE_INSERTS,
		NULL,
		err,
		MAKELANGID(LANG_NEUTRAL, SUBLANG_DEFAULT),
		reinterpret_cast<LPSTR>(&msgBuf),
		0,
		NULL
	);

	std::string msg(msgBuf, s);

	LocalFree(msgBuf);

	return msg;
}
#endif

#ifdef _WIN32
//TODO: Crossplat
void PrintProcessErrorHelp(DWORD code)
{
	switch (code)
	{
	case ERROR_FILE_NOT_FOUND:
		std::cout << "The launcher couldn't find Gladius.exe" << std::endl;
		std::cout << "Please make sure Electool is located next to Gladius.exe" << std::endl;
		break;
	case ERROR_ACCESS_DENIED:
		std::cout << "The launcher was denied access when attempting to launch Gladius.exe" << std::endl;
		break;
	case ERROR_NOT_ENOUGH_MEMORY:
		std::cout << "There wasn't enough system memory to launch Gladius. This is pretty bad." << std::endl;
		break;
	case ERROR_ELEVATION_REQUIRED:
		std::cout << "Electool requires administrator rights to run Gladius." << std::endl;
		std::cout << "You likely have compatibility options set on Gladius.exe that have flagged it to always launch as administrator." << std::endl;
		std::cout << "To fix this, you must either remove the compatibility options (right click > Properties > Compatibility) or run Electool as administrator as well (right click > Run as Administrator)." << std::endl;
		break;
	default:
		std::cout << "No help string for this error.";
		break;
	}
}

//TODO: Crossplat
PROCESS_INFORMATION StartGame()
{
	std::cout << "Starting Gladius..." << std::endl;

	PROCESS_INFORMATION pInfo;

	STARTUPINFO sInfo = { sizeof(sInfo) };
	bool bResult = CreateProcess(L"Gladius.exe",
		nullptr,
		nullptr,
		nullptr,
		false,
		NORMAL_PRIORITY_CLASS | CREATE_SUSPENDED,
		nullptr,
		nullptr,
		&sInfo,
		&pInfo);

	if (!bResult)
	{
		DWORD code = GetLastError();
		std::cerr << "Failed to start Gladius!" << std::endl;
		std::cerr << "Error: " << GetErrorString(code) << "(" << code << ")" << std::endl;
		std::cout << "Information:" << std::endl;
		PrintProcessErrorHelp(code);
		Pause();
		return pInfo;
	}

	return pInfo;
}

//TODO: Crossplat
bool Inject(HANDLE hTarget)
{
	std::cout << "Allocating memory in game process..." << std::endl;

	LPVOID pMem = VirtualAllocEx(hTarget,
		nullptr,
		strlen(pszDLL) + 1,
		MEM_RESERVE | MEM_COMMIT,
		PAGE_EXECUTE_READWRITE);
	if (!pMem)
	{
		std::cerr << "Failed to allocate memory in process!" << std::endl;
		DWORD code = GetLastError();
		std::cerr << "Error: " << GetErrorString(code) << "(" << code << ")" << std::endl;
		return false;
	}

	std::cout << "Writing DLL into game memory..." << std::endl;
	BOOL bWriteSuccess = WriteProcessMemory(
		hTarget,
		pMem,
		pszDLL,
		strlen(pszDLL) + 1,
		nullptr);
	if (!bWriteSuccess)
	{
		std::cerr << "Failed to write " << pszDLL << " into memory!" << std::endl;
		DWORD code = GetLastError();
		std::cerr << "Error: " << GetErrorString(code) << "(" << code << ")" << std::endl;
		return false;
	}

	HMODULE hKrnl = GetModuleHandle(L"kernel32.dll");
	if (hKrnl == NULL)
	{
		std::cerr << "Failed to get Kernel32 handle!" << std::endl;
		DWORD code = GetLastError();
		std::cerr << "Error: " << GetErrorString(code) << "(" << code << ")" << std::endl;
		return false;
	}

	LPVOID loadLib = GetProcAddress(hKrnl, "LoadLibraryA");

	std::cout << "Creating remote thread..." << std::endl;
	HANDLE hLoadLib = CreateRemoteThread(hTarget, nullptr, 0, LPTHREAD_START_ROUTINE(loadLib), pMem, 0, nullptr);
	if (hLoadLib == NULL)
	{
		std::cerr << "Failed to create thread to load library!" << std::endl;
		DWORD code = GetLastError();
		std::cerr << "Error: " << GetErrorString(code) << "(" << code << ")" << std::endl;
		return false;
	}

	std::cout << "Waiting..." << std::endl;
	WaitForSingleObject(hLoadLib, INFINITE);

	std::cout << "Done!" << std::endl;

	std::cout << "Cleaning up..." << std::endl;

	VirtualFreeEx(
		hTarget, pMem, strlen(pszDLL) + 1,
		MEM_RELEASE);

	CloseHandle(hLoadLib);

	return true;
}
#endif

int main()
{
#if _WIN32
	SetWindowText(GetConsoleWindow(), L"Electool Launcher 2.0");
#endif

	PROCESS_INFORMATION process = StartGame();

	if (process.hProcess == NULL || process.hProcess == INVALID_HANDLE_VALUE)
		exit(-1);

#if _DEBUG
	Pause();
#endif

	bool bInjectSuccess = Inject(process.hProcess);

	if (!bInjectSuccess)
	{
		Pause();
		ResumeThread(process.hThread);

		TerminateProcess(process.hProcess, 1);

		CloseHandle(process.hThread);
		CloseHandle(process.hProcess);
		exit(-2);
	}
	else
	{
		std::cout << "Success! Unsuspending Gladius..." << std::endl;

		ResumeThread(process.hThread);

		CloseHandle(process.hThread);
		CloseHandle(process.hProcess);
	}

	exit(0);
}